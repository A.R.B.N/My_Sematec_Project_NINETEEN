package ir.arbn.www.mysematecprojectnineteen;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        findViewById(R.id.openDialog).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(HomeActivity.this, DialogActivity.class);
        startActivity(intent);
    }
}
